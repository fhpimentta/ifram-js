/* eslint-disable no-console */
/* eslint-disable no-param-reassign */
import { Router } from 'express';
import puppeteer from 'puppeteer';

const homeRouter = Router();

homeRouter.get('/', async (req, res) => {
  const param = req.query.d !== undefined ? req.query.d : '';
  const link = req.query.l !== undefined ? req.query.l : 'https://www.';

  const url = `${link}mercadolivre.com${param}`;

  console.log(`url:${url}`);

  try {
    const browser = await puppeteer.launch();
    const [page] = await browser.pages();

    await page.goto(url, { waitUntil: 'networkidle2' });

    const data = await page.evaluate(() => {
      document.querySelectorAll('a').forEach((a) => {
        const att = document.createAttribute('data-local'); // Create a "class" attribute
        att.value = 'load'; // Set the value of the class attribute
        a.setAttributeNode(att);
        a.target = '_self';
        const urls = a.href.split('mercadolivre.com');
        if (urls.length > 1) {
          a.href = `http://localhost:3333?l=${urls[0]}&d=${urls[1]}`;
        } else {
          a.href = `http://localhost:3333?d=${a.href}`;
        }
      });
      return document.querySelector('*')?.outerHTML;
    });
    // exhibitor-item
    await browser.close();
    return res.end(
      data
        ?.replace(
          '</body>',
          `
              <script>
              
              document.addEventListener("DOMContentLoaded", function(){

                 setTimeout(function (){
                      var anchors = document.getElementsByTagName("a");
                      
                      for (var i = 0; i < anchors.length; i++) {
          
                          if(!anchors[i].hasAttribute('data-local')){
                              anchors[i].target='_self'
                              let url =  anchors[i].href.split('mercadolivre.com')
                          
                              if(url.length > 1) {
                                  anchors[i].href = 'http://localhost:3333?l=' + url[0] + '&d=' + url[1]
                              }else {
                                  anchors[i].href = 'http://localhost:3333?d=' +   anchors[i].href
                              }
                          }
                          
                          
                      }
                 },5000)
                                          
                  
                      });
              
                  document.addEventListener("click", function (e) { 
                      
                      
                    if(e.target.getAttribute("href") == null)
                          {
                              //e.preventDefault();
                          console.log(e.target.parentElement.parentElement.getAttribute("href") );
                          }
                      else{
                          console.log(e.target.getAttribute("href") );
                      }
                  })</script></body>
             
             `,
        )
        .replace('<head>', '<head><meta charset="utf-8" />'),
    );
  } catch (err) {
    console.error(err);
  }
});

export default homeRouter;
