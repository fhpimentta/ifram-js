import React from 'react';
import Iframe from 'react-iframe'



const Home: React.FC = () => {

  

  return (
    <div>
      <Iframe url="http://localhost:3333/"
        position="absolute"
        width="100%"
        height="100%"
        allow="fullscreen"
        styles={{border: "none"}}/>
    </div>
  );
};

export default Home;
