

<h3 align="center">
  Iframe Js
</h3>



## 📥 Instalação e execução

1. Faça um clone desse repositório.
2. Instalar <a href='https://nodejs.org/en/'>NodeJs</a>
2. Instalar <a href='https://yarnpkg.com/getting-started/install'>Yarn</a> ( é possivel rodar o projeto apenas com o npm)


### Backend
1. A partir da raiz do projeto, entre na pasta do backend rodando `cd backend`;
2. Rode `yarn install` para instalar as dependências;
3. Rode `yarn dev:server` para iniciar o servidor.

### Frontend Web

_ps: Antes de executar, lembre-se de iniciar o backend deste projeto_

1. A partir da raiz do projeto, entre na pasta do frontend web rodando `cd frontend`;
2. Rode `yarn install` para instalar as dependências;
3. Rode `yarn start` para iniciar o client.



Feito com ♥ by [FernandoPimenta](https://www.linkedin.com/in/fepimenta/)
